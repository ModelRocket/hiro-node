const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

module.exports = function(handler) {
  var port = process.env._LAMBDA_SERVER_PORT;

  if (!port) {
    port = '10023';
  }

  const host = '0.0.0.0:' + port;

  const server = new grpc.Server();

  const PROTO_PATH = 'lambda.proto';

  const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
    includeDirs: ['node_modules/@modelrocket/hiro-shim', '.']
  });

  server.addService(packageDefinition['Function'], {
    invoke: (arg, callback) => {
      var event = {};
      var context = {};

      try {
        event = JSON.parse(arg.request.payload.toString());
      } catch (e) {}

      try {
        context = JSON.parse(arg.request.context.toString());
      } catch (e) {}

      handler(event, context, function(err, resp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, { payload: Buffer.from(JSON.stringify(resp)) });
        }
      });
    }
  });

  server.bind(host, grpc.ServerCredentials.createInsecure());

  server.start();
};
